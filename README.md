![pile of index cards](/media/pile_of_index_cards.jpg)

# FredGrottOS

This is my personal OS to re-harness my thinking and note-taking to epic portions to re-harness
my adhd creative efforts towards progressively hockey stick upward curves.

## Can I clone and use?

Yup, I would prefer credit if you use it and modify it but that is not required as its under BSD clause 2 copyright license.

# The Components to my personal OS

These are the components to my personal OS with a short description of each one.

## Getting Stuff Done

A Tiddlywiki based CRM for personal tasks, see

http://gsd5.tiddlyspot.com/

## Zettelkatsen

A note taking system designed to organically grow knowledge than make it rady to use. The inventor was able to author 70 books and 400 articles using this system, see:

https://zettelkasten.de/posts/zettelkasten-improves-thinking-writing/

My implementation is in markdown using atom-editor(github).  Bonus, I put my personal notes that are not private here right  in this repo so you can see it in action.

Yes, there is a python script for visualizing the connections, search the Zettlekatsen site to find their github repos.

I do not use the Zettelkatsen software as its not digitially signed and its blocked on the wifi I use and
besides I want something free so that I am not locked into a vendor as I personally depend on this system.


# Zettelkatsen for Beginners

No categories, we do the organic org via keywords and links.

Two components of the ID. Date, than one adds a nummber. If its the first note of the day than it can haave the number zero  or one added to the date. If its a note child continuing the note taking on a pervious note one should have the parent note id and than add alpha letters to the note ID. And, IDs are always at the top of the markdown marked with a hash symbol.

Note, that I use child notes as my atomic principle. Also note that now I have to modify how I do note IDs as what if a child note is created on a different day? So now my note ID becomes date_noteid and child notes are
date_noteid_b on same day and on the different day date_parentdate_noteid_b which makes sense as I can visually see it as far as org in my file names.

Keywords go at the top marked by a number symbol, the hash

You need two boxes. My notes is my inbox for all my reading and note taking. Than, my outerprocessednotes is my outbox that forms my long form content generated from reviewing my inbox of notes.

Also note that I am starting to use outlines in all my notes posts. See the resources for the article link on how to do it.

Okay, I added a sample template in the root of the project to show how to outline each
markdown note.

# Resources

Sorry, no my book git repo has to stay private as it has books I have electronically purchased. Start using archive.org for free copies of books, please.

[Create a Zettelkasten for your Notes to Improve Thinking and Writing](https://zettelkasten.de/posts/zettelkasten-improves-thinking-writing/)

[Different Kinds of Ties Between Notes](https://zettelkasten.de/posts/kinds-of-ties/)

[Zettlekatsen overview](https://zettelkasten.de/posts/overview/)

[Zettlekatsen use outlines as post templates](https://zettelkasten.de/posts/how-i-use-outlines-to-write-any-text/)

# Tools

[atom editor by github](https://atom.io/)

# License

BSD clause 2 copyright 2020 Fred Grott(Fredrick Allan Grott)

# Contact

Some of my social links so that you can contact me or use my email addy of
fred DOT grott @ gmail DOT com. Note, I cannot answer individual programmer or designer questions, use reddit and
stackoverflow to get answers to those individual questions.

[keybase io profile](https://keybase.io/fredgrott)

The keybase profile has my twitter, github, reddit, ycombinator hacker news links as well.

[linkedin profile](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/)

[xing profile](https://www.xing.com/profile/Fred_Grott/cv)

[gitlab profile](https://gitlab.com/fred.grott)
